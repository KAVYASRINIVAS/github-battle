import './App.css';
import Header from './Components/header.jsx';
import Main from './Components/main.jsx'
import BattleContainer from './Components/battleContainer.jsx';
import BattleResult from './Components/battleResult.jsx';

import { Switch, Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <Header />
      <Switch>
        <Route exact path="/" component={Main} />
        <Route path="/battleContainer" component={BattleContainer} />
        <Route
          exact
          path={`/player1=:player1&player2=:player2`}
          render={(props) => (
            <BattleResult {...props} />
          )}
        />
      </Switch>
    </div>
  );
}

export default App;
