import React, { Component } from "react";
import "../styles/header.css";

import { Link } from "react-router-dom";

class Header extends Component {
  render() {
    return (
      <div className="header">
        <Link
          to="/"
          style={{
            textDecoration: "none",
            color: "black",
            fontWeight: "bold",
            marginLeft: "10px",
          }}
        >
          Popular
        </Link>
        <Link
          to="/battleContainer"
          style={{
            textDecoration: "none",
            color: "black",
            fontWeight: "bold",
            marginLeft: "10px",
          }}
        >
          Battle
        </Link>
        <button className="themeButtonLight">🔦</button>
        <button className="themeButtonDark">💡</button>
      </div>
    );
  }
}

export default Header;
