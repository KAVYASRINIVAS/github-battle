import React from "react";
import Card from "./card";
import "../styles/main.css";
import MenuBar from "./menu";

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      jsonData: [],
      isDataLoaded: false,
      activeLanguage: "all",
    };
  }

  componentDidMount() {
    fetch(
      " https://api.github.com/search/repositories?q=stars:%3E1+language:All&sort=stars&order=desc&type=Repositories"
    )
      .then((res) => res.json())
      .then((json) => this.setState({ jsonData: json, isDataLoaded: true }));
  }

  handleCardData = (type) => {
    if (type === "all") {
      fetch(
        " https://api.github.com/search/repositories?q=stars:%3E1+language:All&sort=stars&order=desc&type=Repositories"
      )
        .then((res) => res.json())
        .then((json) => this.setState({ jsonData: json, isDataLoaded: true }));
    } else if (type === "javascript") {
      fetch(
        "https://api.github.com/search/repositories?q=stars:%3E1+language:JavaScript&sort=stars&order=desc&type=Repositories"
      )
        .then((res) => res.json())
        .then((json) =>
          this.setState({ jsonData: json, isDataLoaded: true }, () =>
            console.log(this.state.jsonData)
          )
        );
    } else if (type === "ruby") {
      fetch(
        "https://api.github.com/search/repositories?q=stars:%3E1+language:Ruby&sort=stars&order=desc&type=Repositories"
      )
        .then((res) => res.json())
        .then((json) =>
          this.setState({ jsonData: json, isDataLoaded: true }, () =>
            console.log(this.state.jsonData)
          )
        );
    } else if (type === "java") {
      fetch(
        "https://api.github.com/search/repositories?q=stars:%3E1+language:Java&sort=stars&order=desc&type=Repositories"
      )
        .then((res) => res.json())
        .then((json) =>
          this.setState({ jsonData: json, isDataLoaded: true }, () =>
            console.log(this.state.jsonData)
          )
        );
    } else if (type === "css") {
      fetch(
        "https://api.github.com/search/repositories?q=stars:%3E1+language:CSS&sort=stars&order=desc&type=Repositories"
      )
        .then((res) => res.json())
        .then((json) =>
          this.setState({ jsonData: json, isDataLoaded: true }, () =>
            console.log(this.state.jsonData)
          )
        );
    } else if (type === "python") {
      fetch(
        "https://api.github.com/search/repositories?q=stars:%3E1+language:Python&sort=stars&order=desc&type=Repositories"
      )
        .then((res) => res.json())
        .then((json) =>
          this.setState({ jsonData: json, isDataLoaded: true }, () =>
            console.log(this.state.jsonData)
          )
        );
    }
  };

  render() {
    return (
      <div className="main">
        <MenuBar handleCard={this.handleCardData} />
        {!this.state.isDataLoaded ? (
          <div>Fetching Data...</div>
        ) : (
          <Card cardData={this.state.jsonData.items} />
        )}
      </div>
    );
  }
}

export default Main;
