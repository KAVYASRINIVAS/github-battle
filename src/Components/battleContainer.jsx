import React, { Component } from "react";
import "../styles/battleContainer.css";
import user from "../images/user.png";
import battle from "../images/battle.png";
import winner from "../images/winner.png";

import * as GitHubApi from "./api";
import { Link } from "react-router-dom";

class BattleContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      player1: "",
      player2: "",
      player1Data: [],
      player2Data: [],
      player1DataLoaded: false,
      player2DataLoaded: false,
      // showResult: false,
    };
  }

  handlePlayer1 = (e) => {
    e.preventDefault();
    this.setState({
      player1: e.target.value,
    });
  };

  handlePlayer2 = (e) => {
    e.preventDefault();
    this.setState({
      player2: e.target.value,
    });
  };

  findUser = (playerType, user) => {
    GitHubApi.getUserInfo(user).then((repo) => {
      if (playerType === "player1") {
        this.setState({
          player1Data: repo,
          player1DataLoaded: true,
        });
      } else if (playerType === "player2") {
        this.setState({
          player2Data: repo,
          player2DataLoaded: true,
        });
      }
    });
  };

  removePlayer = (e) => {
    if (e.target.id === "player1") {
      this.setState({
        player1DataLoaded: false,
        player1Data: [],
      });
    } else if (e.target.id === "player2") {
      this.setState({
        player2DataLoaded: false,
        player2Data: [],
      });
    }
  };

  render() {
    const player1 = this.state.player1Data.login;
    const player2 = this.state.player2Data.login;

    return (
      <div className="container">
        <span className="heading">Instructions</span>
        <section className="instruction-container">
          <div className="instructions">
            <span>Enter two Github Users</span>
            <img
              style={{
                backgroundColor: "bisque",
                padding: "25px",
                margin: "10px",
                borderRadius: "5px",
              }}
              src={user}
              alt=""
            />
          </div>

          <div className="instructions">
            <span>Battle</span>
            <img
              style={{
                backgroundColor: "bisque",
                padding: "25px",
                margin: "10px",
                borderRadius: "5px",
              }}
              src={battle}
              alt=""
            />
          </div>

          <div className="instructions">
            <span>See the winner</span>
            <img
              style={{
                backgroundColor: "bisque",
                padding: "25px",
                margin: "10px",
                borderRadius: "5px",
              }}
              src={winner}
              alt=""
            />
          </div>
        </section>

        <section className="players-conatiner">
          <span className="heading">Players</span>
          <div className="players">
            {this.state.player1DataLoaded === false ? (
              <form
                onSubmit={(e) => {
                  e.preventDefault();
                  this.findUser("player1", this.state.player1);
                  this.setState({
                    player1: "",
                  });
                }}
              >
                <label>Player One</label>

                <div>
                  <input
                    className="username"
                    type="text"
                    value={this.state.player1}
                    placeholder="github username"
                    onChange={this.handlePlayer1}
                  />

                  <button
                    type="submit"
                    className="submitBtn"
                    disabled={!this.state.player1}
                  >
                    SUBMIT
                  </button>
                </div>
              </form>
            ) : (
              <div>
                <label>Player One</label>
                <div
                  className="players"
                  style={{
                    display: "flex",
                    justifyContent: "flex-start",
                    alignItems: "center",
                    backgroundColor: "bisque",
                    width: "300px",
                    height: "50px",
                    marginRight: "20px",
                  }}
                >
                  <img
                    src={this.state.player1Data.avatar_url}
                    alt=""
                    style={{
                      borderRadius: "50%",
                      height: "30px",
                      width: "30px",
                    }}
                  />
                  <a
                    href={this.state.player1Data.html_url}
                    style={{
                      textDecoration: "none",
                      fontWeight: "bold",
                      color: "brown",
                      marginLeft: "20px",
                    }}
                  >
                    {this.state.player1Data.login}
                  </a>
                  <button
                    id="player1"
                    onClick={this.removePlayer}
                    style={{
                      backgroundColor: "brown",
                      borderRadius: "50%",
                      fontWeight: "bold",
                      marginLeft: "auto",
                      color: "white",
                      height: "25px",
                      marginRight: "20px",
                    }}
                  >
                    X
                  </button>
                </div>
              </div>
            )}

            {this.state.player2DataLoaded === false ? (
              <form
                onSubmit={(e) => {
                  e.preventDefault();
                  this.findUser("player2", this.state.player2);
                  this.setState({
                    player2: "",
                  });
                }}
              >
                <label>Player Two</label>

                <div>
                  <input
                    className="username"
                    type="text"
                    value={this.state.player2}
                    placeholder="github username"
                    onChange={this.handlePlayer2}
                  />

                  <button
                    type="submit"
                    className="submitBtn"
                    disabled={!this.state.player2}
                  >
                    SUBMIT
                  </button>
                </div>
              </form>
            ) : (
              <div>
                <label>Player Two</label>
                <div
                  className="players"
                  style={{
                    display: "flex",
                    justifyContent: "flex-start",
                    alignItems: "center",
                    backgroundColor: "bisque",
                    width: "300px",
                    height: "50px",
                    marginRight: "20px",
                  }}
                >
                  <img
                    src={this.state.player2Data.avatar_url}
                    alt=""
                    style={{
                      borderRadius: "50%",
                      height: "30px",
                      width: "30px",
                    }}
                  />
                  <a
                    href={this.state.player2Data.html_url}
                    style={{
                      textDecoration: "none",
                      fontWeight: "bold",
                      color: "brown",
                      marginLeft: "20px",
                    }}
                  >
                    {this.state.player2Data.login}
                  </a>
                  <button
                    id="player2"
                    onClick={this.removePlayer}
                    style={{
                      backgroundColor: "brown",
                      borderRadius: "50%",
                      fontWeight: "bold",
                      marginLeft: "auto",
                      color: "white",
                      height: "25px",
                      marginRight: "20px",
                    }}
                  >
                    X
                  </button>
                </div>
              </div>
            )}
          </div>
          <>
            <Link
              to={`/player1=${player1}&player2=${player2}`}
              style={{
                margin: "20px",
                borderRadius: "5px",
                backgroundColor: "black",
                color: "white",
                height: "auto",
                width: "auto",
                padding: "10px",
                textAlign: "center",
                alignSelf: "center",
                textDecoration: "none",
                display:
                  this.state.player1DataLoaded && this.state.player2DataLoaded
                    ? "block"
                    : "none",
              }}
            >
              BATTLE
            </Link>
          </>
        </section>
      </div>
    );
  }
}

export default BattleContainer;
