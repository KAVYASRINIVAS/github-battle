import React, { Component } from "react";
import "../styles/menu.css";

const FILTERS = [
  {
    type: "all",
    name: "All",
  },
  {
    type: "javascript",
    name: "JavaScript",
  },
  {
    type: "ruby",
    name: "Ruby",
  },
  {
    type: "java",
    name: "Java",
  },
  {
    type: "css",
    name: "CSS",
  },
  {
    type: "python",
    name: "Python",
  },
];
class MenuBar extends Component {
  render() {
    return (
      <div className="menu">
        {FILTERS.map((filterType) => {
          return (
            <button
              key={filterType.type}
              onClick={() => this.props.handleCard(filterType.type)}
            >
              {filterType.name}
            </button>
          );
        })}
      </div>
    );
  }
}

export default MenuBar;
