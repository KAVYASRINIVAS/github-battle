export function getUserRepos() {
  return fetch("https://api.github.com/users").then((res) => res.json());
}

export function getUserInfo(user) {
  return fetch(`https://api.github.com/users/${user}`).then((res) =>
    res.json()
  );
}
